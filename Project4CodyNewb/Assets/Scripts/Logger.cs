﻿using UnityEngine;
using System.Collections;
using System.IO;

public static class Logger
{

    public static bool logValues;

    public static void Log(string toLog, bool writeLog = false)
    {
        if (logValues)
        {
            Debug.Log(toLog);

            if (writeLog)
            {
                WriteString(toLog);
            }
        }
    }

    static void WriteString(string toWrite)
    {
        //write the information to an external file

        //Find the file path
        FileInfo fileInfo = new FileInfo(Application.dataPath + "/LOG_FILE.txt");
        //Create a file
        if (!fileInfo.Exists)
        {
            using (StreamWriter sw = fileInfo.CreateText())
            {
                sw.WriteLine("File create " + System.DateTime.Now.ToString());
            }
        }

        //Write information to the file
        using (StreamWriter sw = fileInfo.AppendText())
        {
            sw.WriteLine(toWrite);
        }


    }

    public static void LogA(string toLog, bool writeLog = false)
    {
        bool storedLog = logValues;
        logValues = true;
        Log(toLog, writeLog);
        logValues = storedLog;
    }

}
