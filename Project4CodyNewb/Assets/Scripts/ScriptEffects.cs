﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[System.Serializable]
public class ScriptEffects
{
    public EffectTypes type;

    public string name;

    public float duration;
    public float intensity;

    public Image splatter;


    public bool fadeIn;
    public bool fadeOut;


}
