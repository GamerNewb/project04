﻿public enum MovementTypes
{    
    STRAIGHT,
    BEZIER,
    WAIT 
}

public enum FacingTypes
{
    LOOKAT,
    FREELOOK,
    LOOKANDRETURN,
    WAIT 
}

public enum EffectTypes
{
    SHAKE,
    SPLATTER,
    FADE,
    WAIT
}
