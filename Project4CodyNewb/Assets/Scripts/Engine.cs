﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Engine : MonoBehaviour
{
    public List<ScriptEffects> effectList;
    public List<ScriptMovement> movementList;
    public List<ScriptFacings> facingList;

    int currentWaypoint;

    GameObject player;
    public int lastMove = 0, lastFacing = 0, lastEffect = 0;

    void Start()
    {
        player = GameObject.FindWithTag("Player");

        StartCoroutine("MovementEngine");
        StartCoroutine("FacingEngine");
        StartCoroutine("EffectEngine");
        Logger.LogA("Hello!", true);
    }

    IEnumerator MovementEngine()
    {
        for(int i = 0; i < movementList.Count; i++)
        {
            switch(movementList[i].type)
            {
                case MovementTypes.STRAIGHT:
                    yield return StartCoroutine(StraightLine(i));
                    break;

                case MovementTypes.BEZIER:
                    yield return StartCoroutine(BezierCurve(i));
                    break;
                case MovementTypes.WAIT:
                    yield return new WaitForSeconds(movementList[i].wait);
                    break;

            }
        }

        yield return null;
    }

    IEnumerator FacingEngine()
    {
        for (int i = 0; i < facingList.Count; i++)
        {
            switch (facingList[i].type)
            {
                case FacingTypes.FREELOOK:
                    GetComponent<MouseLook>().enabled = true;
                    yield return new WaitForSeconds(facingList[i].time);
                    GetComponent<MouseLook>().enabled = false;
                    break;
                case FacingTypes.LOOKAT:
                    GetComponent<MouseLook>().enabled = false;
                    break;
                case FacingTypes.LOOKANDRETURN:
                    GetComponent<MouseLook>().enabled = false;
                    break;
                case FacingTypes.WAIT:
                    yield return new WaitForSeconds(facingList[i].time);
                    break;

            }
        }
    }

    IEnumerator EffectEngine()
    {
        for (int i = 0; i < effectList.Count; i++)
        {
            switch (effectList[i].type)
            {
                case EffectTypes.FADE:
                    break;

            }
        }

        yield return null;
    }

    IEnumerator StraightLine(int waypointNum)
    {
        float step = 0.0f;
        Vector3 startPos = transform.position;
        Vector3 endPos = movementList[waypointNum].endPoint.transform.position;
        float time = movementList[waypointNum].speed;

        while(step < time)
        {
            transform.position = Vector3.Lerp(startPos, endPos, (step / time));
            step += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        transform.position = endPos;
    }

    IEnumerator LookAtTarget(float time, Vector3 lookPosition)
    {
        float step = 0.0f;
        Quaternion startRot = player.transform.rotation;
        Quaternion targetRot = Quaternion.LookRotation((lookPosition - player.transform.position).normalized);
        while(step < time)
        {
            player.transform.rotation = Quaternion.Lerp(startRot, targetRot, (step / time));
            step += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        //yield wait for the seconds looking at them
        step = 0.0f;
        while (step < time)
        {
            player.transform.rotation = Quaternion.Lerp(targetRot, Quaternion.Euler(player.transform.forward), (step / time));
            step += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        player.transform.rotation = Quaternion.Euler(player.transform.forward);
    }

    IEnumerator BezierCurve(int waypointNum)
    {
        float step = 0.0f;
        Vector3 endPos = movementList[waypointNum].endPoint.transform.position;
        Vector3 startPos = transform.position;
        Vector3 curvePos = movementList[waypointNum].curvePoint.transform.position;
        float time = movementList[waypointNum].speed;

        while (step < time)
        {
            transform.position = GetPoint(startPos, endPos, curvePos, (step/time));
            step += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        transform.position = endPos;
    }


    /// <summary> GetPoint(args)
    /// Gets a point along a Bezier Curve
    /// </summary>
    /// <param name="start">Start point on the curve.</param>
    /// <param name="end">End point on the curve.</param>
    /// <param name="curve">Handle for the curve.</param>
    /// <param name="t">Steps along the curve.</param>
    /// <returns>The point we are after.</returns>
    public Vector3 GetPoint(Vector3 start, Vector3 end, Vector3 curve, float t)
    {
        t = Mathf.Clamp01(t);
        float oneMinusT = 1.0f - t;
        return oneMinusT * oneMinusT * start + 2.0f * oneMinusT * t * curve + t * t * end;
    }
}

