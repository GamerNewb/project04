﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class ScriptMovement
{
    public MovementTypes type;

    public string name;

    public GameObject endPoint;
    public float speed;

    public GameObject curvePoint;

    public float wait;
}
