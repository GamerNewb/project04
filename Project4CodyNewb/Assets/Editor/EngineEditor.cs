﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(Engine))]
public class EngineEditor : Editor
{
    Engine engineScript;

    void Awake()
    {
        engineScript = (Engine)target;
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        //display a button to launch window
        if (GUILayout.Button("Open Editor"))
        {
            EngineWindow.Init();
        }

        serializedObject.ApplyModifiedProperties();
    }

    void PrintInformation()
    {
        Debug.Log("printing movement engine info");
        Debug.Log("movement length" + engineScript.movementList.Count);

        foreach(ScriptMovement movescript in engineScript.movementList)
        {
            Debug.Log("\tMovement printing. . .");
            Debug.Log("\t" + movescript.type.ToString() + ".");
            Debug.Log("\tEnd Point Name: " + movescript.endPoint.gameObject.name + ".");
        }
    }
}
