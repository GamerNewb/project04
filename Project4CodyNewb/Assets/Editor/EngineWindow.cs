﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
using UnityEngine.UI;


public class EngineWindow : EditorWindow
{
    public List<ScriptEffects> effectList;
    public List<ScriptMovement> movementList;
    public List<ScriptFacings> facingList;

    Engine engineScript;

    //Track items we are editing
    int movementItem = 0, facingItem = 0, effectItem = 0;

    //This is the size of the engine window - Cody
    public Vector2 minimumSize = new Vector2(750F, 350F);

    //setup button skin
    GUIStyle miniLeft, miniCenter, miniRight;

    //position variables
    float offsetX = 0f, offsetY = 0f, ELEMENT_HEIGHT = 17f;

    public static void Init()
    {
        EngineWindow window = (EngineWindow)EditorWindow.GetWindow(typeof(EngineWindow));
        window.Show();
    }

    void OnFocus()
    {
        //get a reference to the script we are supposed to be editing
        engineScript = GameObject.Find("EngineObject").GetComponent<Engine>();

        //setup buttons for prettyness and things
        miniLeft = new GUIStyle(EditorStyles.miniButtonLeft);
        miniCenter = new GUIStyle(EditorStyles.miniButtonMid);
        miniRight = new GUIStyle(EditorStyles.miniButtonRight);

        //pull information from the scropt we are editing into local variables
        movementList = engineScript.movementList;
        effectList = engineScript.effectList;
        facingList = engineScript.facingList;

        //pull current waypoint
        movementItem = engineScript.lastMove;
        facingItem = engineScript.lastFacing;
        effectItem = engineScript.lastEffect;

        //make sure nothing is null
        if (movementList == null)
            movementList = new List<ScriptMovement>();
        if (effectList == null)
            effectList = new List<ScriptEffects>();
        if (facingList == null)
            facingList = new List<ScriptFacings>();

        //make sure that there is at least one element in the list
        if (movementList.Count < 1)
            movementList.Add(new ScriptMovement());
        if (effectList.Count < 1)
            effectList.Add(new ScriptEffects());
        if (facingList.Count < 1)
            facingList.Add(new ScriptFacings());
    }

    void OnGUI()
    {
        //built-in variable for editor window
        minSize = minimumSize;

        offsetX = 0;
        offsetY = 0f;
        MovementGUI();
        EffectsGUI();
        FacingsGUI();
    }

    void MovementGUI()
    {
        //setup ects for showing information (x, y, width, height)
        Rect movementLabelPos = new Rect(50, 10, 250f, ELEMENT_HEIGHT);
        offsetY += ELEMENT_HEIGHT;

        //setup a draw line
        Vector2 linePointTop = new Vector2(250, 3);
        Vector2 linePointBottom = new Vector2(250, 350);
        Drawing.DrawLine(linePointTop, linePointBottom, Color.black, 1f, true);

        EditorGUI.LabelField(movementLabelPos, "MOVEMENT WAYPOINTS");

        //notating what thingy we're on
        Rect numberPos = new Rect(115f, offsetY + 10f, 60f, ELEMENT_HEIGHT);
        offsetY += 10f + ELEMENT_HEIGHT;
        EditorGUI.LabelField(numberPos, "" + (movementItem + 1) + "/" + movementList.Count);


        //waypoint name
        Rect nameLabelPos = new Rect(offsetX, offsetY, 110f, ELEMENT_HEIGHT);
        
        EditorGUI.LabelField(nameLabelPos, "Waypoint Name: ");

        //Input field
        Rect nameInputPos = new Rect(offsetX + 110f, offsetY, 135f, ELEMENT_HEIGHT);
        offsetY += ELEMENT_HEIGHT;

        movementList[movementItem].name = EditorGUI.TextField(nameInputPos, movementList[movementItem].name);

        //waypoint type label


        //waypoint type
        Rect waypointPos = new Rect(offsetX + 15f, offsetY + 8f, 220f, ELEMENT_HEIGHT);
        movementList[movementItem].type = (MovementTypes)EditorGUI.EnumPopup(waypointPos, movementList[movementItem].type);
        offsetY += ELEMENT_HEIGHT * 2f;

        //specific Display Info
        switch(movementList[movementItem].type)
        {
            case MovementTypes.STRAIGHT:
                Rect straightStartLabel = new Rect(offsetX, offsetY, 100F, ELEMENT_HEIGHT);
                offsetX += 100f;
                EditorGUI.LabelField(straightStartLabel, "End Point: ");

                Rect straightStartPos = new Rect(offsetX, offsetY, 150, ELEMENT_HEIGHT);
                offsetX -= 100;
                movementList[movementItem].endPoint = (GameObject)EditorGUI.ObjectField(straightStartPos, movementList[movementItem].endPoint, typeof(GameObject),true);

                offsetY += ELEMENT_HEIGHT;

                //Sets the label for the bezier speed - Cody
                Rect straightSpeedStartLabel = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                offsetX += 100f;
                EditorGUI.LabelField(straightSpeedStartLabel, "Speed: ");


                //Set the field to add time in - Cody
                Rect straightSpeedStartPos = new Rect(offsetX, offsetY, 130, ELEMENT_HEIGHT);
                offsetY += ELEMENT_HEIGHT;
                offsetX -= 100f;
                movementList[movementItem].speed = EditorGUI.FloatField(straightSpeedStartPos, movementList[movementItem].speed);
                break;

            case MovementTypes.BEZIER:
                // sets the label for the bezier end point - Cody
                Rect endpStartLabel = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                offsetX += 100f;
                EditorGUI.LabelField(endpStartLabel, "End Point: ");

                //Set the field to drop a game object in - Cody
                Rect bezierendpStartPos = new Rect(offsetX, offsetY, 150, ELEMENT_HEIGHT);
                offsetX -= 100;
                movementList[movementItem].endPoint = (GameObject)EditorGUI.ObjectField(bezierendpStartPos, movementList[movementItem].endPoint, typeof(GameObject), true);

                //Moves the field down
                offsetY += ELEMENT_HEIGHT;

                //Sets the label for the bezier curve point - Cody
                Rect curveStartLabel = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                EditorGUI.LabelField(curveStartLabel, "Curve Point: ");

                offsetX += 100f;
                //Set the field to drop a game object in - Cody
                Rect beziercurveStartPos = new Rect(offsetX, offsetY, 150, ELEMENT_HEIGHT);
                offsetY += ELEMENT_HEIGHT;
                offsetX -= 100f;
                movementList[movementItem].curvePoint = (GameObject)EditorGUI.ObjectField(beziercurveStartPos, movementList[movementItem].curvePoint, typeof(GameObject), true);

                //Sets the label for the bezier speed - Cody
                Rect speedStartLabel = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                offsetX += 100f;
                EditorGUI.LabelField(speedStartLabel, "Speed: ");

                //Moves the field down
                

                //Set the field to add time in - Cody
                Rect bezierspeedStartPos = new Rect(offsetX, offsetY, 130, ELEMENT_HEIGHT);
                offsetY += ELEMENT_HEIGHT;
                offsetX -= 100f;
                movementList[movementItem].speed = EditorGUI.FloatField(bezierspeedStartPos, movementList[movementItem].speed);

                break;

            case MovementTypes.WAIT:
                // sets the label for the bezier end point - Cody
                Rect timeStartLabel = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                offsetX += 100f;
                EditorGUI.LabelField(timeStartLabel, "Time: ");

                //Set the field to add time in - Cody
                Rect waitTimeStartPos = new Rect(offsetX, offsetY, 130, ELEMENT_HEIGHT);
                offsetX -= 100;
                movementList[movementItem].wait = EditorGUI.FloatField(waitTimeStartPos, movementList[movementItem].wait);


                break;


        }

        offsetX += 30;
        Rect prevPos = new Rect(offsetX, 180f, 50f, ELEMENT_HEIGHT + 8f);
        Rect addPos = new Rect(offsetX + 50f, 180f, 90f, ELEMENT_HEIGHT + 8f);
        Rect nextPos = new Rect(offsetX + 140f, 180f, 50f, ELEMENT_HEIGHT + 8f);
        //display 3 little buttons
        if (movementItem != 0)
        {
            if (GUI.Button(prevPos, "Prev", miniLeft))
            {
                //if (movementItem == 0)
                //    movementItem--;
                movementItem--;
            } 
        }
        if (GUI.Button(addPos, "Add", miniCenter))
        {
            movementList.Insert(movementItem + 1, new ScriptMovement());
        }
        if (movementItem != movementList.Count - 1)
        {
            if (GUI.Button(nextPos, "Next", miniRight))
            {
                movementItem++;
            } 
        }//end waypoint switching

        Rect deleteRect = new Rect(offsetX + 35f, 180f + ELEMENT_HEIGHT + 18f, 120f, ELEMENT_HEIGHT + 8f);
        GUI.color = Color.red + Color.white;
        if (movementList.Count > 1)
        {
            if (GUI.Button(deleteRect, "Delete Waypoint"))
            {
                if (movementItem == movementList.Count - 1)
                {
                    movementList.RemoveAt(movementItem);
                    movementItem--;
                }
                else if (movementList.Count > 1)
                {
                    movementList.RemoveAt(movementItem);
                }
                else
                {
                    Debug.Log("Cannot delete last waypoint");
                }
            } 
        }

        GUI.color = Color.white;
    }
    void FacingsGUI()
    {
        Vector2 linePointTop = new Vector2(500, 3);
        Vector2 linePointBottom = new Vector2(500, 350);
        Drawing.DrawLine(linePointTop, linePointBottom, Color.black, 1f, true);
        offsetX = 255;
        offsetY = 0;

        Rect facingTitlePos = new Rect(offsetX + 50f, offsetY + 10f, 240f, ELEMENT_HEIGHT);
        EditorGUI.LabelField(facingTitlePos, "FACING WAYPOINTS");
        offsetY += ELEMENT_HEIGHT;

        //Seeing what waypoint out of total waypoints (x/y) we're on
        Rect numberPos = new Rect(115f + offsetX, offsetY + 10f, 60f, ELEMENT_HEIGHT);
        offsetY += 10f + ELEMENT_HEIGHT;
        EditorGUI.LabelField(numberPos, "" + (facingItem + 1) + "/" + facingList.Count);

        //waypoint name
        Rect nameLabelPos = new Rect(offsetX, offsetY, 105f, ELEMENT_HEIGHT);
        EditorGUI.LabelField(nameLabelPos, "Waypoint Name: ");

        //facing name
        Rect facingNamePos = new Rect(offsetX + 110f, offsetY, 130f, ELEMENT_HEIGHT);
        facingList[facingItem].name = EditorGUI.TextField(facingNamePos, facingList[facingItem].name);
        offsetY += ELEMENT_HEIGHT;

        //facing type
        Rect facingTypeLabelPos = new Rect(offsetX, offsetY + 5f, 95f, ELEMENT_HEIGHT);
        offsetX += 80f;
        EditorGUI.LabelField(facingTypeLabelPos, "FacingType: ");

        Rect facingTypePos = new Rect(offsetX, offsetY + 5f, 155f, ELEMENT_HEIGHT);
        offsetX -= 80f;
        offsetY += ELEMENT_HEIGHT * 2f;
        facingList[facingItem].type = (FacingTypes)EditorGUI.EnumPopup(facingTypePos, facingList[facingItem].type);


        // THIS IS WHERE I GO TO PUT THINGS UNDER THE DROPDOWN -- ADD MORE CASES - Cody
        switch (facingList[facingItem].type)
        {
            case FacingTypes.LOOKAT:

                //display a target to look at
                Rect windowDisplay = new Rect(offsetX, offsetY, 110f, ELEMENT_HEIGHT);
                offsetX += 110f;
                EditorGUI.LabelField(windowDisplay, "Target to Look at: ");

                windowDisplay = new Rect(offsetX, offsetY, 130f, ELEMENT_HEIGHT);
                offsetX -= 110f;//maybe gotta change this
                offsetY += ELEMENT_HEIGHT;
                facingList[facingItem].lookTarget = (GameObject)EditorGUI.ObjectField(windowDisplay, facingList[facingItem].lookTarget, typeof(GameObject), true);

                //facing time
                windowDisplay = new Rect(offsetX, offsetY, 115f, ELEMENT_HEIGHT);
                offsetX += 130f;
                EditorGUI.LabelField(windowDisplay, "Time to look over: ");

                windowDisplay = new Rect(offsetX, offsetY, 110f, ELEMENT_HEIGHT);
                offsetX -= 130f;
                offsetY += ELEMENT_HEIGHT;
                facingList[facingItem].time = EditorGUI.FloatField(windowDisplay, facingList[facingItem].time);
                break;

            case FacingTypes.FREELOOK:

                // sets the label for the bezier end point - Cody
                Rect timeStartLabel = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                offsetX += 100f;
                EditorGUI.LabelField(timeStartLabel, "Time: ");

                //Set the field to add time in - Cody
                Rect waitTimeStartPos = new Rect(offsetX, offsetY, 130, ELEMENT_HEIGHT);
                offsetX -= 100;
                facingList[facingItem].time = EditorGUI.FloatField(waitTimeStartPos, facingList[facingItem].time);

                break;

            case FacingTypes.LOOKANDRETURN:
                //display a target to look at
                //Rect  = new Rect(offsetX, offsetY, 110f, ELEMENT_HEIGHT);
                //offsetX += 110f;
                //EditorGUI.LabelField(windowDisplay, "Target to Look at: ");

                //windowDisplay = new Rect(offsetX, offsetY, 130f, ELEMENT_HEIGHT);
                //offsetX -= 110f;//maybe gotta change this
                //offsetY += ELEMENT_HEIGHT;
                //facingList[facingItem].lookTarget = (GameObject)EditorGUI.ObjectField(windowDisplay, facingList[facingItem].lookTarget, typeof(GameObject), true);

                ////facing time
                //windowDisplay = new Rect(offsetX, offsetY, 115f, ELEMENT_HEIGHT);
                //offsetX += 130f;
                //EditorGUI.LabelField(windowDisplay, "Time to look over: ");

                //windowDisplay = new Rect(offsetX, offsetY, 110f, ELEMENT_HEIGHT);
                //offsetX -= 130f;
                //offsetY += ELEMENT_HEIGHT;
                //facingList[facingItem].time = EditorGUI.FloatField(windowDisplay, facingList[facingItem].time);
                break;

            case FacingTypes.WAIT:

                // sets the label for the bezier end point - Cody
                Rect facingWaitStartLabel = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                offsetX += 100f;
                EditorGUI.LabelField(facingWaitStartLabel, "Time: ");

                //Set the field to add time in - Cody
                Rect facingWaitTimeStartPos = new Rect(offsetX, offsetY, 130, ELEMENT_HEIGHT);
                offsetX -= 100;
                facingList[facingItem].time = EditorGUI.FloatField(facingWaitTimeStartPos, facingList[facingItem].time);


                break;
        }

        //3 buttons
        offsetX += 30;
        Rect prevPos = new Rect(offsetX, 180f, 50f, ELEMENT_HEIGHT + 8f);
        Rect addPos = new Rect(offsetX + 50f, 180f, 90f, ELEMENT_HEIGHT + 8f);
        Rect nextPos = new Rect(offsetX + 140f, 180f, 50f, ELEMENT_HEIGHT + 8f);
        //display 3 little buttons
        //previous
        if (facingItem != 0)
        {
            if (GUI.Button(prevPos, "Prev", miniLeft))
            {
                facingItem--;
            }
        }
        //add
        if (GUI.Button(addPos, "Add", miniCenter))
        {
            facingList.Insert(facingItem + 1, new ScriptFacings());
        }
        //next
        if (facingItem != facingList.Count - 1)
        {
            if (GUI.Button(nextPos, "Next", miniRight))
            {
                facingItem++;
            }
        }//end waypoint switching

        Rect deleteRect = new Rect(offsetX + 35f, 180f + ELEMENT_HEIGHT + 18f, 120f, ELEMENT_HEIGHT + 8f);
        GUI.color = Color.red + Color.white;
        if (facingList.Count > 1)
        {
            if (GUI.Button(deleteRect, "Delete Waypoint"))
            {
                if (facingItem == facingList.Count - 1)
                {
                    facingList.RemoveAt(facingItem);
                    facingItem--;
                }
                else if (facingList.Count > 1)
                {
                    facingList.RemoveAt(facingItem);
                }
                else
                {
                    Debug.Log("Cannot delete last waypoint");
                }
            }
        }

        GUI.color = Color.white;

    }
    void EffectsGUI()
    {
        offsetX = 500;
        offsetY = 0;

        // Set the title at the top of the section - Cody
        Rect effectTitlePos = new Rect(offsetX + 50f, offsetY + 10f, 240f, ELEMENT_HEIGHT);
        EditorGUI.LabelField(effectTitlePos, "EFFECTS WAYPOINTS");
        offsetY += ELEMENT_HEIGHT;

        //Seeing what waypoint out of total waypoints (x/y) we're on - Cody
        Rect numberPos = new Rect(115f + offsetX, offsetY + 10f, 60f, ELEMENT_HEIGHT);
        offsetY += 10f + ELEMENT_HEIGHT;
        EditorGUI.LabelField(numberPos, "" + (effectItem + 1) + "/" + effectList.Count);

        //waypoint name
        Rect nameLabelPos = new Rect(offsetX, offsetY, 105f, ELEMENT_HEIGHT);
        EditorGUI.LabelField(nameLabelPos, "Waypoint Name: ");

        //facing name
        Rect effectNamePos = new Rect(offsetX + 110f, offsetY, 130f, ELEMENT_HEIGHT);
        effectList[effectItem].name = EditorGUI.TextField(effectNamePos, effectList[effectItem].name);
        offsetY += ELEMENT_HEIGHT;

        //facing type
        Rect effectTypeLabelPos = new Rect(offsetX, offsetY + 5f, 95f, ELEMENT_HEIGHT);
        offsetX += 80f;
        EditorGUI.LabelField(effectTypeLabelPos, "FacingType: ");

        Rect effectTypePos = new Rect(offsetX, offsetY + 5f, 155f, ELEMENT_HEIGHT);
        offsetX -= 80f;
        offsetY += ELEMENT_HEIGHT * 2f;
        effectList[effectItem].type = (EffectTypes)EditorGUI.EnumPopup(effectTypePos, effectList[effectItem].type);

        switch (effectList[effectItem].type)
        {
            case (EffectTypes.SHAKE):

                // sets the label for the duration - Cody
                Rect effectDurationStartLabel = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                offsetX += 100f;
                EditorGUI.LabelField(effectDurationStartLabel, "Duration: ");

                //Set the field to add time in - Cody
                Rect effectDurTimeStartPos = new Rect(offsetX, offsetY, 130, ELEMENT_HEIGHT);
                offsetX -= 100;
                effectList[effectItem].duration = EditorGUI.FloatField(effectDurTimeStartPos, effectList[effectItem].duration);

                offsetY += ELEMENT_HEIGHT;

                // sets the label for the intensity - Cody
                Rect effectIntensityStartLabel = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                offsetX += 100f;
                EditorGUI.LabelField(effectIntensityStartLabel, "Intensity: ");
                
                //Set the field to add time in - Cody
                Rect effectIntensityTimeStartPos = new Rect(offsetX, offsetY, 130, ELEMENT_HEIGHT);
                offsetX -= 100;
                effectList[effectItem].intensity = EditorGUI.FloatField(effectIntensityTimeStartPos, effectList[effectItem].intensity);

                

                break;
            case (EffectTypes.FADE):

                // sets the label for the intensity - Cody
                Rect effectfadeinStartLabel = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                offsetX += 100f;
                EditorGUI.LabelField(effectfadeinStartLabel, "Fade In: ");

                //Set the field to add time in - Cody
                Rect effectfadeinBoolStartPos = new Rect(offsetX, offsetY, 130, ELEMENT_HEIGHT);
                offsetX -= 100;
                effectList[effectItem].fadeIn = EditorGUI.Toggle(effectfadeinBoolStartPos, effectList[effectItem].fadeIn);

                offsetY += ELEMENT_HEIGHT;

                // sets the label for the intensity - Cody
                Rect effectfadeoutStartLabel = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                offsetX += 100f;
                EditorGUI.LabelField(effectfadeoutStartLabel, "Fade Out: ");

                //Set the field to add time in - Cody
                Rect effectfadeoutBoolStartPos = new Rect(offsetX, offsetY, 130, ELEMENT_HEIGHT);
                offsetX -= 100;
                effectList[effectItem].fadeOut = EditorGUI.Toggle(effectfadeoutBoolStartPos, effectList[effectItem].fadeOut);

                offsetY += ELEMENT_HEIGHT;

                // sets the label for the duration - Cody
                Rect effectfadeDurationStartLabel = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                offsetX += 100f;
                EditorGUI.LabelField(effectfadeDurationStartLabel, "Duration: ");

                //Set the field to add time in - Cody
                Rect effectfadeTimeStartPos = new Rect(offsetX, offsetY, 130, ELEMENT_HEIGHT);
                offsetX -= 100;
                effectList[effectItem].duration = EditorGUI.FloatField(effectfadeTimeStartPos, effectList[effectItem].duration);

                break;
            case (EffectTypes.SPLATTER):

                Rect effectSplatImageStartLabel = new Rect(offsetX, offsetY, 110f, ELEMENT_HEIGHT);
                offsetX += 110f;
                EditorGUI.LabelField(effectSplatImageStartLabel, "Image: ");

                Rect effectSplatImageStartPos = new Rect(offsetX, offsetY, 130f, ELEMENT_HEIGHT);
                offsetX -= 110f;//maybe gotta change this
                offsetY += ELEMENT_HEIGHT;
                effectList[effectItem].splatter = (Image)EditorGUI.ObjectField(effectSplatImageStartPos, effectList[effectItem].splatter, typeof(Image), true);

                offsetY += ELEMENT_HEIGHT;

                // sets the label for the duration - Cody
                Rect effectSplatDurationStartLabel = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                offsetX += 100f;
                EditorGUI.LabelField(effectSplatDurationStartLabel, "Duration: ");

                //Set the field to add time in - Cody
                Rect effectSplatTimeStartPos = new Rect(offsetX, offsetY, 130, ELEMENT_HEIGHT);
                offsetX -= 100;
                effectList[effectItem].duration = EditorGUI.FloatField(effectSplatTimeStartPos, effectList[effectItem].duration);

                break;
            case (EffectTypes.WAIT):

                // sets the label for the bezier end point - Cody
                Rect effectWaitStartLabel = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                offsetX += 100f;
                EditorGUI.LabelField(effectWaitStartLabel, "Time: ");

                //Set the field to add time in - Cody
                Rect effectWaitTimeStartPos = new Rect(offsetX, offsetY, 130, ELEMENT_HEIGHT);
                offsetX -= 100;
                effectList[effectItem].duration = EditorGUI.FloatField(effectWaitTimeStartPos, effectList[effectItem].duration);

                break;
            default:
                break;

        }

        //3 buttons
        offsetX += 30;
        Rect prevPos = new Rect(offsetX, 180f, 50f, ELEMENT_HEIGHT + 8f);
        Rect addPos = new Rect(offsetX + 50f, 180f, 90f, ELEMENT_HEIGHT + 8f);
        Rect nextPos = new Rect(offsetX + 140f, 180f, 50f, ELEMENT_HEIGHT + 8f);
        //display 3 little buttons
        //previous
        if (effectItem != 0)
        {
            if (GUI.Button(prevPos, "Prev", miniLeft))
            {
                effectItem--;
            }
        }
        //add
        if (GUI.Button(addPos, "Add", miniCenter))
        {
            effectList.Insert(effectItem + 1, new ScriptEffects());
        }
        //next
        if (effectItem != effectList.Count - 1)
        {
            if (GUI.Button(nextPos, "Next", miniRight))
            {
                effectItem++;
            }
        }//end waypoint switching

        Rect deleteRect = new Rect(offsetX + 35f, 180f + ELEMENT_HEIGHT + 18f, 120f, ELEMENT_HEIGHT + 8f);
        GUI.color = Color.red + Color.white;
        if (effectList.Count > 1)
        {
            if (GUI.Button(deleteRect, "Delete Waypoint"))
            {
                if (effectItem == effectList.Count - 1)
                {
                    effectList.RemoveAt(effectItem);
                    effectItem--;
                }
                else if (effectList.Count > 1)
                {
                    effectList.RemoveAt(effectItem);
                }
                else
                {
                    Debug.Log("Cannot delete last waypoint");
                }
            }
        

        

    }
        GUI.color = Color.white;
}

    void OnLostFocus()
    {
        PushData();
    }

    void PushData()
    {
        //push the stuff back
        engineScript.movementList = movementList;
        engineScript.facingList = facingList;
        engineScript.effectList = effectList;

        //push last waypoints to engine
        engineScript.lastMove = movementItem;
        engineScript.lastFacing = facingItem;
        engineScript.lastEffect = effectItem;
    }
}
